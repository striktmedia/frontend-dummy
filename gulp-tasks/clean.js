var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	del = require('del');


gulp.task('clean', function (callback) {
	del.sync(['build']);
	callback(); // Tell gulp task has finished
});