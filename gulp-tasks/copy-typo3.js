var gulp = require('gulp'),
	config = require('../config'); // Relative to this file

gulp.task('copy-typo3', function () {
	// Copy stylesheets
	gulp.src('.' + config.path.resources.css + '/**/*')
		.pipe(gulp.dest(config.path.typo3ext + '/StyleSheets'));

	// Copy gfx
	gulp.src('.' + config.path.resources.gfx + '/**/*')
		.pipe(gulp.dest(config.path.typo3ext + '/GFX'));

	// Copy javascript
	gulp.src('.' + config.path.resources.js + '/**/*')
		.pipe(gulp.dest(config.path.typo3ext + '/JavaScript'));
});