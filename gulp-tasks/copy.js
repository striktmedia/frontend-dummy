var gulp = require('gulp'),
	config = require('../config'); // Relative to this file

gulp.task('copy', function () {
	// Copy assets content folder
	gulp.src(config.path.resources.source + '/**/*')
		.pipe(gulp.dest(config.path.resources.build));
});