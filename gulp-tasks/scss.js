var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	autoprefixer = require('gulp-autoprefixer'),
	browserSync = require('browser-sync'),
	gutil = require('gulp-util'),
	plumber = require('gulp-plumber'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	sassGlob = require('gulp-sass-glob'),
	sourcemaps = require('gulp-sourcemaps');


var onError = function (error) {
	//gutil.beep();
	gutil.log(gutil.colors.red(error));
};


gulp.task('scss', function () {
	gulp.src(config.path.scss.source + '/*.{scss,sass}')
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(autoprefixer({
			browsers: ['last 2 versions']
		}))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(config.path.scss.build))
		.pipe(browserSync.stream());
});