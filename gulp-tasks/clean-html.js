var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	del = require('del');


gulp.task('clean-html', function (callback) {
	del(config.path.build + '/*.html');
	callback();
});