var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	del = require('del');


gulp.task('clean-json-nav', function (callback) {
	del(config.path.json + '/nav.json');
	callback();
});