var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	browserSync = require('browser-sync');


gulp.task('browser-sync', function () {
	browserSync.init({
		server: {
			baseDir: config.path.build,
			directory: true,
			open: false
		}
	});
});