var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	scsslint = require('gulp-scss-lint');

gulp.task('scss-lint', function () {
	return gulp.src(config.path.scss.source + '/*.{scss,sass}')
				.pipe(scsslint());
});