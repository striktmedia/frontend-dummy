var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	frontMatter = require('gulp-front-matter'),
	data = require('gulp-data'),
	gutil = require('gulp-util'),
	path = require('path'),
	pluck = require('gulp-pluck'),
	plumber = require('gulp-plumber');


var onError = function (error) {
	gutil.beep();
	gutil.log(gutil.colors.red(error));
};


gulp.task('json-nav', function () {
	gulp.src(config.path.templates.source + '/*.{nunjucks,njs,html}')
		.pipe(plumber({errorHandler: onError}))
		.pipe(frontMatter({property: 'page'}))
		.pipe(data(function (file) {
			file.page.url = gutil.replaceExtension(path.basename(file.path), '.html');
		}))
		.pipe(pluck('page', 'nav.json'))
		.pipe(data(function (file) {
			file.contents = new Buffer(JSON.stringify(file.page));
		}))
		.pipe(gulp.dest(config.path.json));
});