var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	gutil = require('gulp-util'),
	plumber = require('gulp-plumber'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify');


var onError = function (error) {
	gutil.beep();
	gutil.log(gutil.colors.red(error));
};


gulp.task('js', function () {
	//	Loop over the config javascript keys
	for (var key in config.javascript) {

		gulp.src(config.javascript[key])
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(concat(config.key + '.' + key + '.js'))
			.pipe(uglify())
			.pipe(rename({
				basename: config.key + '.' + key,
				suffix: '.min'
			}))
			.pipe(gulp.dest(config.path.js.build))
			.pipe(browserSync.stream());

	}

});