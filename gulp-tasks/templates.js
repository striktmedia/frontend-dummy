var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	browserSync = require('browser-sync'),
	data = require('gulp-data'),
	gutil = require('gulp-util'),
	htmlmin = require('gulp-htmlmin'),
	frontMatter = require('gulp-front-matter'),
	fs = require('fs'),
	nunjucks = require('gulp-nunjucks-api'),
	path = require('path'),
	prettify = require('gulp-prettify'),
	plumber = require('gulp-plumber'),
	spy = require('through2-spy');


var onError = function (error) {
	gutil.beep();
	gutil.log(gutil.colors.red(error));
};


gulp.task('templates', function () {
	// Declare timer variable
	var timer,
		oData = [];

	// Start timer function to check of nav.json exist
	function startTimer() {
		timer = setInterval(function(){
			gutil.log(gutil.colors.blue('*** Start timer ***'));
			fs.stat(config.path.json + '/nav.json', function(err, stat) {
				if(err == null) { // nav.json exists
					gutil.log(gutil.colors.green('Nav.json exists'));
					stopTimer();
					buildingTemplates();
				} else { // nav.json doesn't exists
					gutil.log(gutil.colors.red('Nav.json doesn\'t exists, error code: ' + err.code));
				}
			});
		}, 500);
	}

	// Stop timer function to disable the setInterval
	function stopTimer() {
		gutil.log(gutil.colors.blue('*** Clear timer ***'));
		clearInterval(timer);
	}

	// Gulp tasks to build the html
	function buildingTemplates() {

		for (var key in config.data) {
			oData[key] = JSON.parse(fs.readFileSync(config.data[key]));
		}

		return gulp.src(config.path.templates.source + '/*.{nunjucks,html,njs}')
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(data(function () {
				return oData;
			}))
			.pipe(frontMatter({
				property: 'data.page'
			}))
			.pipe(nunjucks({
				src: [
					config.path.templates.source,
					config.path.templates.partials,
					config.path.templates.layouts
				],
				data: config,
				verbose: false
			}))
			//.pipe(spy.obj(function (file) {
			//	console.log(file.data);
			//}))
			.pipe(htmlmin({
				collapseWhitespace: true
			}))
			.pipe(prettify({
				indent_size: 2,
				indent_with_tabs: true,
				indent_level: 10,
				indent_inner_html: true
			}))
			.pipe(gulp.dest(config.path.build))
			.pipe(browserSync.reload({
				stream: true,
				once: true
			}));


	}

	/* Trigger start timer function to begin the process of checken if
	 * nav json exists and if it exists build the html
	 */
	startTimer();

});
