var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	gutil = require('gulp-util');

gulp.task('watch', function () {
	// Report changes to the console to give information feedback about the process
	function reportChange(e) {
		gutil.log(gutil.template('File <%= file %> was <%= type %>, rebuilding...', {
			file: gutil.colors.cyan(e.path),
			type: e.type
		}));
	}

	if (!gulp.slurped) {

		// Watch scss files
		gulp.watch(config.path.scss.source + "/**/*.{scss,sass}", ['scss'])
			.on('change', reportChange);

		// Watch javascript files
		gulp.watch(config.path.js.source + "/**/*.{js,coffee,ts}", ['js'])
			.on('change', reportChange);

		// Watch template files
		gulp.watch(config.path.templates.source + '/**/*.nunjucks', ['html'])
			.on('change', reportChange);

		// Watch assets files
		gulp.watch(config.path.resources.source + '/**/*', ['copy'])
			.on('change', reportChange);

		gulp.slurped = true; // step 3

	}
});