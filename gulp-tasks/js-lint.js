var gulp = require('gulp'),
	config = require('../config'), // Relative to this file
	jscs = require('gulp-jscs');
	//eslint = require('gulp-eslint');


gulp.task('js-lint', function () {
	gulp.src(config.path.js.source + '/**/*.js')
		.pipe(jscs())
		.pipe(jscs.reporter());
});

//gulp.task('js-lint', function () {
//	gulp.src(config.path.js.source + '/**/*.js')
//		.pipe(eslint())
//		.pipe(eslint.format());
//		// Brick on failure to be super strict
//		//.pipe(eslint.failOnError());
//});