var gulp = require('gulp'),
	config = require('./config.json'),
	plumber = require('gulp-plumber'),
	requireDir = require('require-dir'),
	runSequence = require('run-sequence');


// I added a property to the gulp object called slurped (get it?) and inialized it to
// be false. Then I only add the watch tasks if gulp.slurped === false. I changed gulp.
// slurped at the end of the watch task to true. (Note: make sure you add the slurped
// property outside of the watch task)
gulp.slurped = false;


// ------------------------------------------------------------------------------------ //
// Require Gulp tasks
// - Pulling in all tasks from the tasks folder
// ------------------------------------------------------------------------------------ //
requireDir('./gulp-tasks', { recurse: true });


// ------------------------------------------------------------------------------------ //
// Collection Task: Check
// - Check javascript code with eslint
// - Check scss code with scss-lint
// ------------------------------------------------------------------------------------ //
gulp.task('check', function (callback) {
	runSequence(
		'scss-lint',
		'js-lint',
		callback);
});


// ------------------------------------------------------------------------------------ //
// Collection Task: HTML
// - Deletes nav.json
// - Create nav.json from yaml in nunjucks templates
// - Deletes build folder
// - Compile html from nunjucks templates
// ------------------------------------------------------------------------------------ //
gulp.task('html', function (callback) {
	runSequence(
		'clean-json-nav',
		'json-nav',
		'clean-html',
		'templates',
		callback);
});


// ------------------------------------------------------------------------------------ //
// Collection Task: Default
//  @note runs also on "gulp"
// - Runs html collection task
// - Create nav.json from yaml in nunjucks templates
// - Start BrowserSync for local server
// - Runs watch task for watching changes on files
// ------------------------------------------------------------------------------------ //
gulp.task('default', function (callback) {
	runSequence(
		'clean',
		'html',
		['copy', 'scss', 'js'],
		'browser-sync',
		'watch',
		callback);
});